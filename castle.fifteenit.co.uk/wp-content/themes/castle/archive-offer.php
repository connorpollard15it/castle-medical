<?php
/**
* Header Template
* @file					index.php
* @filesource			wp-content/themes//gprojects.php


*/
?>

<?php get_header(); ?>
	<div id="content-<?php the_ID();?>">  

	<div class="center"><h1><?php the_field('offers_title','options')?></h1>
    <?php the_field('offers_content','options')?></div>
        <div class="wrap">
            <div class="flexwrapped">
                <?php if ( have_posts() ) { ?>
                   <?php while ( have_posts() ) { ?>
                        <?php the_post();?>
               
               
                   	 
             <div class="project2">
                            
                              
                                    <a href="https://www.castle.fifteenit.co.uk/get-in-touch/">
                                         <div class="offer_overlay">
                                       		<div class="titlename"><h2>  <?php the_title();?> </h2> </div>
                                            <p> <?php the_content();?> </p>
                                       </div>
                                    </a>
                                    </div>
                                   
            <?php } ?>
      <?php } ?>
                                
          </div>
         </div>
            </div>
<?php get_footer();?>