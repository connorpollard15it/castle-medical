<?php

/**

 * Index Template

 * @file           404.php

 * @package        Castle Medical Training

 * @filesource     wp-content/themes/fifteenitnewark/404.php

 * @since          Castle Medical Training 1.0

*/

get_header();?>

	<div class="body wrapper">

		<div class="content">

    

		<?php if (have_posts()) { ?>

            <?php while (have_posts()) { ?>

            	<?php the_post();?>

            		<article class="page">

                    	<?php if (get_field('404_page_title','option')) { ?>

                            <h1 class="page-title"><?php the_field('404_page_title','option'); ?></h1>

                        <?php } ?>

            			<section class="page-content">

                        	<?php the_content();?>

                        </section>

            		</article>

            <?php } ?>

        <?php } ?>    

		</div>

	</div>

<?php get_footer();?>