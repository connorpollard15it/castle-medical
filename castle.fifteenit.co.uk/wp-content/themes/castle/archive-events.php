<?php
/**
* Header Template
* @file					index.php
* @filesource			wp-content/themes//gprojects.php


*/
?>

<?php get_header(); ?>
	<div id="content-<?php the_ID();?>">  

	<div class="center"><h1><?php the_field('events_title','options')?></h1>
    <?php the_field('events_content','options')?></div>
        <div class="wrap">
            <div class="flexwrapped">
                <?php if ( have_posts() ) { ?>
                   <?php while ( have_posts() ) { ?>
                        <?php the_post();?>
               
               
                   	 
             <div class="project1">
                            
                               <?php if ( has_post_thumbnail() ) { ?>
                                    <a href="<?php the_permalink();?>">
                                      <?php the_post_thumbnail('image_events');?>
                                       <div class="project_overlay">
                                       		<div class="titlename"><h2>  <?php the_title();?> </h2> </div>
                                       </div>
                                    </a>
                                    </div>
                                    <?php }?>
            <?php } ?>
      <?php } ?>
                                
          </div>
         </div>
            </div>
<?php get_footer();?>