<?php

/**

 * Index Template

 * @file           page-get-in-touch.php

 * @package        Castle Medical Training

 * @filesource     wp-content/themes/castle/page-get-in-touch.php

 * @since          Castle Medical Training 1.0

*/

get_header();?>

	<div class="body wrapper">  

		<?php if (have_posts()) { ?>

            <?php while (have_posts()) { ?>

            	<?php the_post();?>

                    <?php $lat = get_field('lat','options');?>

                    <?php $long = get_field('longitude','options');?>

                    <?php $address = get_field('company_address','options'); ?>

                    <?php $addresssan = sanitize_text_field( $address );?>

                    <div class="content">

                        <article class="page">

                            <h1 class="page-title"><a class="page-link" rel="bookmark" href="<?php the_permalink();?>"><?php the_title();?></a></h1>

                            	<div class="flexwrapper">

                                	<div class="even block">

                                		<?php the_content();?>
                                        
                                        <?php echo do_shortcode( '[contact-form-7 id="251" title="Contact form 1"]' ); ?>

                                  	</div>

                                   	<div class="even block">

                                    	<div class="companyinfo">

										<?php if (get_field('copyright_name','options')) { ?>

                                            <p><a href="<?php echo get_home_url(); ?>" rel="home"><?php the_field('copyright_name','options');?></a></p>

                                        <?php } ?>

                                        <?php if (get_field('company_number','option') ) { ?>

                                            <p><strong>Company No:</strong> <?php the_field('company_number','option');?><br/></p>

                                        <?php } ?>

                                        <?php if (get_field('company_address','option') ) { ?>

                                            <p><i class="fa fa-map-marker" aria-hidden="true"></i><span class="value"><?php the_field('company_address','option');?></span></p>

                                        <?php } ?>

                                            <?php if (get_field('contact_number','option')) { ?>

                                            <?php $phone = get_field('contact_number','option'); ?>

                                            <?php $countryCode = '44'; ?>

                                            <?php $tel = preg_replace("/[^0-9]/", "", $phone); ?>

                                            <?php $tel = preg_replace('/^0?/', '+'.$countryCode, $tel); ?>

                                            <p><a class="phone" href="tel:<?php echo $tel ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span class="value"><?php the_field('contact_number','option') ?></span></a></p>

                                        <?php } ?>

                                        <?php if (get_field('contact_email','option')) { ?>

                                            <p><a class="email" href="mailto:<?php the_field('contact_email','option') ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <span class="value"><?php the_field('contact_email','option') ?></span></a></p>

                                        <?php } ?>

                                        </div>

                                	</div>

                               	</div>
                                  	<?php if( have_rows ('page_content') ) { ?>

                	<?php while ( have_rows ('page_content') ) { the_row(); ?>

                    

						<?php if( get_row_layout() == 'full_image_no_text' ) { //Full Image No text ?>

                        

							<?php get_template_part( 'template-parts/full_image' ); ?>

                            

                        <?php } else if ( get_row_layout() == 'full_image_with_text' ) { //Full Image With Text ?>

                            

							<?php get_template_part( 'template-parts/full_image_content' ); ?>



                        <?php } else if ( get_row_layout() == 'block_section_with_no_background_image' ) { //Block section with no background image ?>



							<?php get_template_part( 'template-parts/text_block' ); ?>

                       

                       	<?php } else if ( get_row_layout() == 'multiple_columns' ) { //Block section with no background image ?>



							<?php get_template_part( 'template-parts/multiple_columns' ); ?>



						<?php } else if ( get_row_layout() == 'display_posts' ) { // Display posts/shortcode section ?>



							<?php get_template_part( 'template-parts/display_posts' ); ?>



						<?php } else if ( get_row_layout() == 'split_column_image' ) { //Split column layout ?>



							<?php get_template_part( 'template-parts/split_column' ); ?>



						<?php } else if ( get_row_layout() == 'gallery' ) { //Gallery section ?>

                        

							<?php get_template_part( 'template-parts/gallery' ); ?>

                            

                            <?php } else if ( get_row_layout() == 'gallery_slider_inc_slider' ) { //Gallery section with links for slides ?> 

                            

                                <?php get_template_part( 'template-parts/slider-links' ); ?>

                        

						<?php } else if ( get_row_layout() == 'gallery_slider' ) { //Gallery section ?> 

                        

							<?php get_template_part( 'template-parts/slider' ); ?>

                        

						<?php } else if ( get_row_layout() == 'tour_dates' ) { //Gallery section ?> 

                        

							<?php get_template_part( 'template-parts/tour_dates' ); ?>

                        

						<?php } else if ( get_row_layout() == 'full_video_no_text' ) { //Gallery section ?> 

                        

							<?php get_template_part( 'template-parts/full_video' ); ?>

                        
						<?php } else if ( get_row_layout() == 'opening_times' ) { //Gallery section ?> 

                        

							<?php get_template_part( 'template-parts/opening_times' ); ?>
                            
                            

							<?php } else if ( get_row_layout() == 'display_logo' ) { //Gallery section ?> 

                        

							<?php get_template_part( 'template-parts/display_logo' ); ?>
                        

                		<?php } ?>

                        

						<?php wp_reset_postdata();?>

                        

                        

                	<?php } ?>

                <?php } ?>

            

        

                        </article>

                    </div>

                     
            <?php } ?>

        <?php } ?>    

	</div>

<?php get_footer();?>