jQuery(function($){

	($)(window).on('load',function() {
		var viewportWidth = $(window).width();
		if (viewportWidth < 800) {
			$(".section").addClass("pp-scrollable");
		}
	 });

	$(window).resize(function() {
		var viewportWidth = $(window).width();
		if (viewportWidth < 800) {
			$(".section").addClass("pp-scrollable");
		}
	});	
});


/*jQuery(document).ready(function($) {
			
	$('.pagepiling').pagepiling({
        direction: 'horizontal',
        verticalCentered: true,
		navigation: {
            'bulletsColor': '#fff',
            'position': 'right',
        },
		normalScrollElements: '.pp-scrollable',
		touchSensitivity: 25,
	});
});*/