	<?php                            
        $bgimage = get_sub_field('full_image');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)
        $border = get_sub_field('border_location');
        if( $bgimage ) { ?>
            <div class="section section-background image-only 
                <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="<?php if (get_sub_field('border_location')){?>
                    <?php if (in_array('top', get_sub_field('border_location'))) {?>
                        border-top-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>
                        border-bottom-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('left', get_sub_field('border_location'))) {?>
                        border-left-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                    <?php if (in_array('right', get_sub_field('border_location'))) {?>
                        border-right-color: <?php the_sub_field('border'); ?>;
                    <?php } ?>
                <?php } ?>"
				<?php if (get_sub_field( 'anchor' ) ) { ?>
                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"
<?php } ?>
				>
                <!--<div class="pp-tableCell" style="height:100%">-->
                    <img src="<?php echo wp_get_attachment_image_url( $bgimage, $size );?>"/>
                <!--</div>-->
            </div>
        <?php }
    ?>
    <?php wp_reset_postdata();?>
