	<?php 

        $setColor =  get_sub_field('background_colour', $post->ID);

        $color = $setColor;

        $rgb = hex2rgba($color);

        $rgba = hex2rgba($color, 1);

        $border = get_sub_field('border_location');

    ?>

        <?php if ( $rgba ) { ?>

            <?php if ( get_sub_field('text_colour' ) ) { ?>

                <div class="section  colourbg <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;color:<?php the_sub_field('text_colour');?>;<?php if (get_sub_field('border')){?>

                    <?php if (in_array('top', get_sub_field('border_location'))) {?>

                        border-top-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>

                        border-bottom-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('left', get_sub_field('border_location'))) {?>

                        border-left-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('right', get_sub_field('border_location'))) {?>

                        border-right-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                <?php } ?>"

				<?php if (get_sub_field( 'anchor' ) ) { ?>

                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"

                <?php } ?>

				>

            <?php } else { ?>

                <div class="section  colourbg <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="background:<?php echo $rgba ?>;<?php if (get_sub_field('border')){?>

                    <?php if (in_array('top', get_sub_field('border_location'))) {?>

                        border-top-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>

                        border-bottom-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('left', get_sub_field('border_location'))) {?>

                        border-left-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('right', get_sub_field('border_location'))) {?>

                        border-right-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                <?php } ?>"

				<?php if (get_sub_field( 'anchor' ) ) { ?>

                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"

                <?php } ?>

				>

            <?php } ?>

        <?php } else { ?>

            <div class="section  <?php if ($border) { foreach ($border as $border1) { echo ' '.$border1; } }?>" style="

            <?php if (get_sub_field('border')){?>

                    <?php if (in_array('top', get_sub_field('border_location'))) {?>

                        border-top-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('bottom', get_sub_field('border_location'))) {?>

                        border-bottom-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('left', get_sub_field('border_location'))) {?>

                        border-left-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                    <?php if (in_array('right', get_sub_field('border_location'))) {?>

                        border-right-color: <?php the_sub_field('border'); ?>;

                    <?php } ?>

                <?php } ?>"

				<?php if (get_sub_field( 'anchor' ) ) { ?>

                    data-anchor="<?php the_sub_field( 'anchor' ); ?>"

                <?php } ?>

				>

        <?php } ?>

		<?php if (get_sub_field('slanted_edges') == "Top" || get_sub_field('slanted_edges') == "Both" ) {?>

            <div class="slanted-top" style=" <?php if (get_sub_field('slanted_top_section_angle')) { ?>transform:skewY(<?php the_sub_field('slanted_top_section_angle');?>deg);<?php } ?><?php if(get_sub_field('slanted_top_section_colour')) { ?>background:<?php the_sub_field('slanted_top_section_colour');?><?php } ?>"></div>

        <?php } ?>

        	<div class="pp-tableCell" style="height:100%">

                <div class="content">

                    <div class="">

                        <?php if (get_sub_field('section_title')) { ?>

                            <h2 class="section-title"><?php the_sub_field('section_title');?></h2>

                        <?php } ?>

                       <div class="section-text"> <?php if (get_sub_field('section_content')) { ?>

                            <?php the_sub_field('section_content');?>
                            </div>

                        <?php } ?>

                    </div>

              	</div>

            </div>

        </div>

