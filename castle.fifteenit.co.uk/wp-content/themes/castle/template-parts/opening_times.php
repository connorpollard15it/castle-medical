<div class="content2">
<?php

	if( get_field('opening') == '' ) {?>
         <div class="opening_time_box">   
         <h2> Opening times </h2>
         	<button class="opening_button" onclick="myFunction()">Click here for opening times for today</button>
            
	        <p id="times"></p>
        
        </div>   
	<?php } ?>
	
	<script>
	function myFunction() {
	  var d = new Date();
	  var weekday = new Array(7);
	  weekday[0] = "Sunday - <?php if (get_field('sundays_opening_time','option')) { ?><?php the_field('sundays_opening_time','option') ?> <?php } ?>";
	  weekday[1] = "Monday - <?php if (get_field('mondays_opening_time','option')) { ?><?php the_field('mondays_opening_time','option') ?> <?php } ?>";
	  weekday[2] = "Tuesday - <?php if (get_field('tuesdays_opening_time','option')) { ?><?php the_field('tuesdays_opening_time','option') ?> <?php } ?>";
	  weekday[3] = "Wednesday - <?php if (get_field('wednesdays_opening_time','option')) { ?><?php the_field('wednesdays_opening_time','option') ?> <?php } ?>";
	  weekday[4] = "Thursday - <?php if (get_field('thursdays_opening_time','option')) { ?><?php the_field('thursdays_opening_time','option') ?> <?php } ?> ";
	  weekday[5] = "Friday - <?php if (get_field('fridays_opening_times','option')) { ?><?php the_field('fridays_opening_times','option') ?> <?php } ?>";
	  weekday[6] = "Saturday - <?php if (get_field('saturdays_opening_times','option')) { ?><?php the_field('saturdays_opening_times','option') ?> <?php } ?>";
	
	  var n = weekday[d.getDay()];
	  document.getElementById("times").innerHTML = n;
	}
	</script>

</div>


