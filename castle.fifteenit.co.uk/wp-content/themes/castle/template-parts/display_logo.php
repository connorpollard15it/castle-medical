<div class="section pp-scrollable colourbg shortcode flexwrapper column " style="background:rgba(255,255,255,1);">
	<div class="logo-box">
    
    	<div class="content2">
        
        	<div class="main-image">
            
            	<?php 
$image = get_sub_field('main_image');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    echo wp_get_attachment_image( $image, $size );
}
?>
            </div>
        </div>
        <div class="logo-overlay">
        
        	<div class="image">
            <?php 
$image = get_sub_field('image');
$size = 'full'; // (thumbnail, medium, large, full or custom size)
if( $image ) {
    echo wp_get_attachment_image( $image, $size );
}?>
            
            </div>
        </div>
    </div>
</div>